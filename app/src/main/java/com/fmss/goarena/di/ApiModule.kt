package com.fmss.goarena.di

import android.content.Context
import android.util.Log
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fmss.goarena.BuildConfig
import com.fmss.goarena.core.UserManager
import com.fmss.goarena.network.GoArenaService
import com.fmss.goarena.network.GoRequesttInterceptor
import com.fmss.goarena.network.NetworkConstants
import com.fmss.goarena.network.RestResponseCallAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.CookieJar
import okhttp3.JavaNetCookieJar
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import java.net.CookieManager
import java.net.CookiePolicy
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit
import javax.inject.Qualifier

/**
 * Created by nikinci on 1/30/21.
 */
@Module
@InstallIn(SingletonComponent::class)
object ApiModule {
    const val BASE_URL = "BASE_URL"
    const val SSL_CERTIFICATE = "SSL_CERTIFICATE"
    public const val LOG_TAG_NETWORK = "GOArena-Network"
    const val CONNECT_TIMEOUT_IN_SECONDS_PROD: Long = 20
    const val READ_TIMEOUT_IN_SECONDS_PROD: Long = 30
    const val WRITE_TIMEOUT_IN_SECONDS_PROD: Long = 30

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class BaseUrl

    @Provides
    fun provideGoArenaService(
        converterFactory: JacksonConverterFactory,
        okHttpClient: OkHttpClient, @BaseUrl
        mBaseUrl: String
    ): GoArenaService {
        return Retrofit.Builder()
            .addCallAdapterFactory(RestResponseCallAdapterFactory.create())
            .addConverterFactory(converterFactory)
            .baseUrl(mBaseUrl)
            .client(okHttpClient)
            .build().create(GoArenaService::class.java)
    }

    @Provides
    @BaseUrl
    fun provideBaseUrl(): String {
        return NetworkConstants.BASE_URL
    }

    @Provides
    fun provideCookieJar(): CookieJar {
        val cookieManager = CookieManager()
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL)
        return JavaNetCookieJar(cookieManager)
    }

    @Provides
    fun provideJacksonConverterFactory(objectMapper: ObjectMapper): JacksonConverterFactory {
        return JacksonConverterFactory.create(objectMapper)
    }


    @Provides
    fun provideObjectMapper(): ObjectMapper {
        return ObjectMapper().apply {
            setSerializationInclusion(JsonInclude.Include.NON_NULL)
            configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            configure(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL, true)
            //tood set dateformat
            dateFormat = SimpleDateFormat()
        }
    }

    @Provides
    fun provideOkHttpClient(
        cookieJar: CookieJar, @ApplicationContext context: Context, userManager: UserManager
    ): OkHttpClient {
        val clientBuilder = OkHttpClient.Builder()
        clientBuilder.cookieJar(cookieJar)

        clientBuilder.connectTimeout(CONNECT_TIMEOUT_IN_SECONDS_PROD, TimeUnit.SECONDS)
        clientBuilder.readTimeout(READ_TIMEOUT_IN_SECONDS_PROD, TimeUnit.SECONDS)
        clientBuilder.writeTimeout(WRITE_TIMEOUT_IN_SECONDS_PROD, TimeUnit.SECONDS)
        if (BuildConfig.DEBUG) {
            clientBuilder.addInterceptor(ChuckerInterceptor(context))
            val httpLoggingInterceptor = HttpLoggingInterceptor { message ->
                Log.d(
                    LOG_TAG_NETWORK,
                    message
                )
            }.apply {
                level = HttpLoggingInterceptor.Level.BODY
            }
            clientBuilder.addInterceptor(httpLoggingInterceptor)
            clientBuilder.addInterceptor(GoRequesttInterceptor(userManager))
        }
        return clientBuilder.build()
    }


}

