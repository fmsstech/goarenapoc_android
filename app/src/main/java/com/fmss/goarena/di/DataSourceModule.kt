package com.fmss.goarena.di

import com.fmss.goarena.data.remote.PostRemoteDataSource
import com.fmss.goarena.data.remote.UserRemoteDataSource
import com.fmss.goarena.data.source.PostDataSource
import com.fmss.goarena.data.source.UserDataSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * Created by nikinci on 1/30/21.
 */
@Module
@InstallIn(SingletonComponent::class)
abstract class DataSourceModule {

    @Binds
    abstract fun bindPostDataSource(
        //for mocking set PostMockDataSource
        dataSource: PostRemoteDataSource
    ): PostDataSource


    @Binds
    abstract fun bindUserDataSource(
        //for mocking set PostMockDataSource
        dataSource: UserRemoteDataSource
    ): UserDataSource

}
