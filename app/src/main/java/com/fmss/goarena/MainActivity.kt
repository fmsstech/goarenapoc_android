package com.fmss.goarena

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.fmss.goarena.base.BaseBindingActivity
import com.fmss.goarena.core.UserManager
import com.fmss.goarena.databinding.ActivityMainBinding
import com.fmss.goarena.ui.sharePost.ShareEditPostActivity
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

/**
 * Created by nikinci on 1/30/21.
 */
@AndroidEntryPoint
class MainActivity : BaseBindingActivity<ActivityMainBinding>() {

    companion object {

        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    @Inject
    lateinit var userManager: UserManager

    override fun populateUI(savedInstanceState: Bundle?) {
        val navController = findNavController(R.id.nav_host_fragment)
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home, R.id.navigation_dashboard
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        mBinding?.apply {
            navView.apply {
                setupWithNavController(navController)
            }
            fab.setOnClickListener {
                startActivity(ShareEditPostActivity.newIntent(this@MainActivity))
            }
        }

        navController.addOnDestinationChangedListener { controller: NavController?, destination: NavDestination, arguments: Bundle? ->
            when (destination.id) {
                R.id.navigation_home -> mBinding?.fab?.show()
                R.id.navigation_dashboard -> mBinding?.fab?.hide()
            }
        }

    }

    override fun getContentViewLayoutResId() = R.layout.activity_main
}