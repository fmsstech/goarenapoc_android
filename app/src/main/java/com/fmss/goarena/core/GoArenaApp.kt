package com.fmss.goarena.core

import androidx.multidex.MultiDexApplication
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by nikinci on 1/30/21.
 */
@HiltAndroidApp
class GoArenaApp : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

    }
}