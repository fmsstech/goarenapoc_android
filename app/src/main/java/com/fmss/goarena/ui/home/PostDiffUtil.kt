package com.fmss.goarena.ui.home

import androidx.recyclerview.widget.DiffUtil
import com.fmss.goarena.model.PostGroup

/**
 * Created by nikinci on 2/1/21.
 */
object PostDiffUtil {


    @JvmStatic
    var POST_ITEM_DIFF_CALLBACK: DiffUtil.ItemCallback<PostGroup> =
        object : DiffUtil.ItemCallback<PostGroup>() {
            override fun areItemsTheSame(oldItem: PostGroup, newItem: PostGroup): Boolean {
                return (oldItem.post?.id == newItem.post?.id)
            }

            override fun areContentsTheSame(oldItem: PostGroup, newItem: PostGroup): Boolean {

                return (oldItem.post
                    ?.title == newItem.post?.title) && (oldItem.post?.description == newItem.post?.description) && (oldItem.post?.imageUrl == newItem.post?.imageUrl) && (oldItem.post?.imageStr == newItem.post?.imageStr)
            }

        }
}