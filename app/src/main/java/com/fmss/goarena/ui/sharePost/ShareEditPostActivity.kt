package com.fmss.goarena.ui.sharePost

import android.Manifest
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import coil.load
import com.fmss.goarena.R
import com.fmss.goarena.base.BaseBindingActivity
import com.fmss.goarena.databinding.ActivitySharepostBinding
import com.fmss.goarena.extensions.toBase64
import com.fmss.goarena.model.Post
import com.fmss.goarena.network.ResponseState
import com.yalantis.ucrop.UCrop
import dagger.hilt.android.AndroidEntryPoint
import java.io.File


/**
 * Created by nikinci on 2/1/21.
 */
@AndroidEntryPoint
class ShareEditPostActivity :
    BaseBindingActivity<ActivitySharepostBinding>() {

    companion object {
        private val BUNDLE_KEY_POST = "bundle.key.post"

        fun newIntent(context: Context, post: Post? = null): Intent {
            return Intent(context, ShareEditPostActivity::class.java).apply {
                post?.let {
                    putExtra(BUNDLE_KEY_POST, it)
                }
            }

        }
    }


    val viewModel: ShareEditPostViewModel by viewModels()

    var mCroppedUri: Uri? = null


    private val mPost: Post? by lazy {
        if (intent.hasExtra(BUNDLE_KEY_POST)) {
            intent.getSerializableExtra(BUNDLE_KEY_POST) as Post
        } else {
            null
        }
    }


    val getContent = registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->

        uri?.apply {
            val tempUri: Uri = Uri.fromFile(
                File.createTempFile("goArena", "")
            )
            UCrop.of(this, tempUri)
                .start(this@ShareEditPostActivity)


        }
    }


    private val requestPermission =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (isGranted) {
                getContent.launch("image/*")

            } else {

            }
        }

    override fun getContentViewLayoutResId() = R.layout.activity_sharepost
    override fun populateUI(savedInstanceState: Bundle?) {

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mBinding?.apply {
            mPost?.let {
                this.item = mPost
                if (it.imageStr.isNullOrEmpty()) {
                    imageFrame.visibility = View.GONE
                }
                supportActionBar?.title = getString(R.string.title_update_post)
            }
            buttonAddPhoto.setOnClickListener {
                requestPermission.launch(Manifest.permission.READ_EXTERNAL_STORAGE)
            }
            buttonShare.setOnClickListener {
                checkAndUpload()
            }
        }

    }

    private fun checkAndUpload() {
        var title = mBinding?.edittextTitle?.text.toString()
        var desc = mBinding?.edittextDesc?.text.toString()
        var selectedImageUri = mCroppedUri
        mBinding?.textinputTitle?.error = null
        mBinding?.textinputDesc?.error = null

        if (title.isEmpty() || title.length < 3) {
            mBinding?.textinputTitle?.error = getString(R.string.sharepost_error_title)
            return
        }

        if (desc.isEmpty() || desc.length < 5) {
            mBinding?.textinputDesc?.error = getString(R.string.sharepost_error_desc)
            return
        }

        //post


        viewModel.postResponse.observe(this, Observer {
            when (it) {
                is ResponseState.Loading -> if (it.loading) {
                    showLoading()
                } else {
                    hideLoading()
                }
                is ResponseState.Error -> {// showErrorDialog()
                }
                is ResponseState.Success -> {
                    it.data
                    showSuccessDialog()
                }
            }
        })


        var imageStr: String? = mPost?.imageStr


        mCroppedUri?.let {
            imageStr = it.toBase64()
            mPost?.imageStr = imageStr

        }

        viewModel.sharePost(mPost?.id, title, desc, imageStr)


    }

    private fun showSuccessDialog() {
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setMessage(getString(R.string.sharepost_success_description))
            .setCancelable(false)
            .setPositiveButton(
                getString(R.string.sharepost_success_button)
            ) { dialog, id ->
                dialog.dismiss()
                finish()
            }

        val alert = dialogBuilder.create()
        alert.setTitle(getString(R.string.sharepost_success_title))
        alert.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setCroppedUri(uri: Uri?) {
        mCroppedUri = uri
        uri?.apply {
            mBinding?.let {
                it.imageFrame.load(mCroppedUri)
                it.layoutImage.visibility = View.VISIBLE
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            setCroppedUri(UCrop.getOutput(data!!))
        }
    }
}