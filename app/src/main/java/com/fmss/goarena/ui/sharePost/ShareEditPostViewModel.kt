package com.fmss.goarena.ui.sharePost

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.fmss.goarena.base.BaseViewModel
import com.fmss.goarena.data.repository.PostRepository
import com.fmss.goarena.network.ResponseState
import com.fmss.goarena.network.request.PostRequestDto
import com.fmss.goarena.network.response.PostResponseDto
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by nikinci on 1/30/21.
 */
@HiltViewModel
class ShareEditPostViewModel @Inject constructor(
    handle: SavedStateHandle?, val postRepository: PostRepository
) : BaseViewModel() {

    private val _postResponse =
        MutableLiveData<ResponseState<PostResponseDto>>()
    val postResponse: LiveData<ResponseState<PostResponseDto>> =
        _postResponse

    init {
    }


    fun sharePost(id: Long? = null, title: String, description: String, imageStr: String? = null) {
        viewModelScope.launch {
            val requestDto = PostRequestDto(id, title, description, imageStr)
            postRepository.sendPost(requestDto).collect {
                _postResponse.value = it
            }
        }

    }

}
