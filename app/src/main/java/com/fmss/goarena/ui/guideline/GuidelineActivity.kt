package com.fmss.goarena.ui.guideline

import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.fmss.goarena.R
import com.fmss.goarena.base.BaseBindingActivity
import com.fmss.goarena.databinding.ActivityGuidelineBinding
import com.fmss.goarena.network.ResponseState
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by nikinci on 1/30/21.
 */
@AndroidEntryPoint
class GuidelineActivity :
    BaseBindingActivity<ActivityGuidelineBinding>() {

    val viewModel: GuidelineViewModel by viewModels()
    override fun getContentViewLayoutResId() = R.layout.activity_guideline
    override fun populateUI(savedInstanceState: Bundle?) {
        viewModel.postListResponse.observe(this, Observer {
            when (it) {
                is ResponseState.Loading -> if (it.loading) {
                    //show loader
                } else {
                    //hide loader
                }
                is ResponseState.Error -> {// showErrorDialog()
                }
                is ResponseState.Success -> {
                    it.data
                }
            }
        })
    }
}