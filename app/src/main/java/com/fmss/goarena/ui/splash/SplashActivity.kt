package com.fmss.goarena.ui.splash

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.fmss.goarena.R
import com.fmss.goarena.base.BaseBindingActivity
import com.fmss.goarena.databinding.ActivitySplashBinding
import com.fmss.goarena.ui.login.LoginActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


/**
 * Created by nikinci on 2/1/21.
 */
@AndroidEntryPoint
class SplashActivity :
    BaseBindingActivity<ActivitySplashBinding>() {

    companion object {

        fun newIntent(context: Context): Intent {
            return Intent(context, SplashActivity::class.java)

        }
    }

    override fun getContentViewLayoutResId() = R.layout.activity_splash
    override fun populateUI(savedInstanceState: Bundle?) {

        lifecycleScope.launch {
            delay(3000)
            startActivity(
                LoginActivity.newIntent(this@SplashActivity)
            )
            finish()
        }
    }


}