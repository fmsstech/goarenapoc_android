package com.fmss.goarena.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.fmss.goarena.MainActivity
import com.fmss.goarena.R
import com.fmss.goarena.base.BaseBindingActivity
import com.fmss.goarena.core.UserManager
import com.fmss.goarena.databinding.ActivityLoginBinding
import com.fmss.goarena.extensions.toast
import com.fmss.goarena.model.User
import com.fmss.goarena.network.ResponseState
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


/**
 * Created by nikinci on 2/1/21.
 */
@AndroidEntryPoint
class LoginActivity :
    BaseBindingActivity<ActivityLoginBinding>() {

    companion object {

        fun newIntent(context: Context): Intent {
            return Intent(context, LoginActivity::class.java)

        }
    }

    val viewModel: LoginViewModel by viewModels()

    @Inject
    lateinit var userManager: UserManager

    override fun getContentViewLayoutResId() = R.layout.activity_login
    override fun populateUI(savedInstanceState: Bundle?) {

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mBinding?.apply {

            buttonLogin.setOnClickListener {
                val username = edittextUsername.text.toString()
                val pass = edittextPass.text.toString()
                viewModel.login(username, pass)
            }
        }

        viewModel.loginResponse.observe(this, Observer {
            when (it) {
                is ResponseState.Loading -> if (it.loading) {
                    showLoading()
                } else {
                    hideLoading()
                }
                is ResponseState.Error -> {// showErrorDialog()
                    toast(it.errorMessage)
                }
                is ResponseState.Success -> {
                    val data = it.data
                    userManager.setAuthToken(data.token)
                    userManager.setUser(User(id=data.id, username = data.username))
                    startActivity(MainActivity.newIntent(this))
                }
            }

        })

    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


}