package com.fmss.goarena.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.fmss.goarena.base.BaseViewModel
import com.fmss.goarena.data.repository.UserRepository
import com.fmss.goarena.network.ResponseState
import com.fmss.goarena.network.request.LoginRequestDto
import com.fmss.goarena.network.response.LoginResponseDto
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by nikinci on 1/30/21.
 */
@HiltViewModel
class LoginViewModel @Inject constructor(
    handle: SavedStateHandle?, val userRepository: UserRepository
) : BaseViewModel() {

    private val _loginResponse =
        MutableLiveData<ResponseState<LoginResponseDto>>()
    val loginResponse: LiveData<ResponseState<LoginResponseDto>> =
        _loginResponse

    init {
    }

    fun login(username: String, passsword: String) {
        viewModelScope.launch {
            userRepository.login(LoginRequestDto(username, passsword)).collect {
                _loginResponse.value = it
            }
        }

    }

}
