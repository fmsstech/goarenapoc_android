package com.fmss.goarena.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fmss.goarena.data.repository.PostRepository
import com.fmss.goarena.network.ResponseState
import com.fmss.goarena.network.request.PostListRequestDto
import com.fmss.goarena.network.response.PostListResponseDto
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(val postRepository: PostRepository) : ViewModel() {

    private val _postListResponse =
        MutableLiveData<ResponseState<PostListResponseDto>>()
    val postListResponse: LiveData<ResponseState<PostListResponseDto>> =
        _postListResponse

    var currentPage = 0

    init {
        getPostList()
    }


    fun getPostList() {
        currentPage++
        viewModelScope.launch {
            val requestDto = PostListRequestDto(currentPage)
            postRepository.getPostList(requestDto).collect {
                _postListResponse.value = it
            }
        }
    }


}