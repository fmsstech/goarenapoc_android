package com.fmss.goarena.ui.guideline

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.fmss.goarena.base.BaseViewModel
import com.fmss.goarena.data.repository.PostRepository
import com.fmss.goarena.network.ResponseState
import com.fmss.goarena.network.request.DashboardRequestDto
import com.fmss.goarena.network.response.PostListResponseDto
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by nikinci on 1/30/21.
 */
@HiltViewModel
class GuidelineViewModel @Inject constructor(
    handle: SavedStateHandle?, val postRepository: PostRepository
) : BaseViewModel() {

    private val _postListResponse =
        MutableLiveData<ResponseState<PostListResponseDto>>()
    val postListResponse: LiveData<ResponseState<PostListResponseDto>> =
        _postListResponse

    init {
        getPostList()
    }


    private fun getPostList() {
        viewModelScope.launch {
            val requestDto = DashboardRequestDto(timeStr = "adsd")
            postRepository.getCmsBulk(requestDto).collect {
                _postListResponse.value = it
            }
        }
    }

}
