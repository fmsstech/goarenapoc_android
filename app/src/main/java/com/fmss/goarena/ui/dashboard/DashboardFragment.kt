package com.fmss.goarena.ui.dashboard

import android.graphics.Color
import android.view.View
import androidx.fragment.app.viewModels
import com.fmss.goarena.R
import com.fmss.goarena.base.BaseBindingFragment
import com.fmss.goarena.databinding.FragmentDashboardBinding
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class DashboardFragment : BaseBindingFragment<FragmentDashboardBinding>() {

    val dashboardViewModel: DashboardViewModel by viewModels()

    override fun populateUI(rootView: View) {
        setBarChart()
        setPieChart()
        setLineChart()

    }

    private fun setLineChart() {
        val entries = mutableListOf<Entry>()
        val range = 10

        for (i in 0 until 5) {
            entries.add(Entry(i.toFloat(), ((Math.random() * range) + range / 5).toFloat()))
        }


        val lineDataSet = LineDataSet(entries, "Line Chart")

        val data = LineData(lineDataSet)
        mBinding?.lineChart?.apply {
            setData(data)
            invalidate()
            description.text = "I am a line chart!"
        }
    }

    private fun setBarChart() {
        val valueList = mutableListOf<Double>()
        val entries = mutableListOf<BarEntry>()

        for (i in 0..4) {
            valueList.add(i * 100.1)
        }
        for (i in 0 until valueList.size) {
            val barEntry = BarEntry(i.toFloat(), valueList[i].toFloat())
            entries.add(barEntry)
        }

        val barDataSet = BarDataSet(entries, "Bar Chart")

        val data = BarData(barDataSet)
        mBinding?.barChart?.apply {
            setData(data)
            invalidate()
            description.text = "I am a bar chart!"
        }
    }


    private fun setPieChart() {
        val valueList = mutableListOf<Double>()
        val entries = mutableListOf<PieEntry>()
        val title = "Title"

        for (i in 0..3) {
            valueList.add(i * 100.1)
        }
        val range = valueList.size

        for (i in 0 until range) {
            val barEntry = PieEntry(((Math.random() * range) + range / 5).toFloat())
            entries.add(barEntry)
        }


        val dataSet = PieDataSet(entries, "Pie Chart")

        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter())
        data.setValueTextSize(11f)
        data.setValueTextColor(Color.WHITE)
        dataSet.setDrawIcons(false)

        val colors: ArrayList<Int> = ArrayList()

        for (c in ColorTemplate.VORDIPLOM_COLORS) colors.add(c)

        for (c in ColorTemplate.JOYFUL_COLORS) colors.add(c)

        for (c in ColorTemplate.COLORFUL_COLORS) colors.add(c)

        for (c in ColorTemplate.LIBERTY_COLORS) colors.add(c)

        for (c in ColorTemplate.PASTEL_COLORS) colors.add(c)

        colors.add(ColorTemplate.getHoloBlue())

        dataSet.colors = colors
        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f

        mBinding?.pieChart?.apply {
            setData(data)
            invalidate()
            description.text = "I am a pie chart!"
        }
    }

    override fun getContentLayoutResId() = R.layout.fragment_dashboard


}