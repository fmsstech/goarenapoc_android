package com.fmss.goarena.ui.loading

import android.content.Context
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatDialog
import com.fmss.goarena.R

/**
 * Created by nikinci on 2/1/21.
 */

class GoArenaLoadingDialog(context: Context) :
    AppCompatDialog(context) {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutRes())

        setCancelable(false)
        setCanceledOnTouchOutside(false)

        populateUi()
    }

    private fun populateUi() {


    }


    @LayoutRes
    protected open fun getLayoutRes(): Int {
        return R.layout.layout_dialog_loading
    }


}