package com.fmss.goarena.ui.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fmss.goarena.R
import com.fmss.goarena.core.UserManager
import com.fmss.goarena.databinding.ItemPostBinding
import com.fmss.goarena.databinding.ItemPostLoadingMoreBinding
import com.fmss.goarena.model.Post
import com.fmss.goarena.model.PostGroup
import com.fmss.goarena.model.User
import com.fmss.goarena.ui.home.PostDiffUtil.POST_ITEM_DIFF_CALLBACK
import javax.inject.Inject

/**
 * Created by nikinci on 2/1/21.
 */

class PostAdapter @Inject constructor(
    var userManager: UserManager
) : ListAdapter<PostGroup, RecyclerView.ViewHolder>(POST_ITEM_DIFF_CALLBACK) {
    companion object {
    }

    val RECYCLER_ITEM_TYPE_LOAD_MORE = 0
    val RECYCLER_ITEM_TYPE_POST = 1
    var uiCallBack: PostListUICallBack? = null

    val LOADINGMORE_ITEM_ID = Long.MAX_VALUE

    interface PostListUICallBack {
        fun onClickEdit(item: Post)
    }

    @NonNull
    override fun onCreateViewHolder(
        @NonNull parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        when (viewType) {
            RECYCLER_ITEM_TYPE_POST -> {
                val binding: ItemPostBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_post, parent, false
                )
                binding.uiCallback = uiCallBack
                return PostViewHolder(binding)
            }
            RECYCLER_ITEM_TYPE_LOAD_MORE -> {
                val binding: ItemPostLoadingMoreBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_post_loading_more, parent, false
                )
                return LoadingMoreViewHolder(binding)
            }
            else -> {
                throw Exception("invalid type")
            }
        }
    }


    override fun getItemViewType(position: Int): Int {
        return when (getItem(position).post?.id) {
            LOADINGMORE_ITEM_ID -> RECYCLER_ITEM_TYPE_LOAD_MORE
            else -> RECYCLER_ITEM_TYPE_POST

        }
    }

    override fun onBindViewHolder(@NonNull holder: RecyclerView.ViewHolder, position: Int) {
        val item = getItem(position)

        if (holder is PostViewHolder) {
            holder.setItem(item, userManager.user.value)
        } else if (holder is LoadingMoreViewHolder) {
            //auto start loading
        }
    }


    class PostViewHolder(var binding: ItemPostBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun setItem(item: PostGroup, user: User?) {
            binding.item = item
            binding.user = user

        }
    }


    class LoadingMoreViewHolder(binding: ItemPostLoadingMoreBinding) :
        RecyclerView.ViewHolder(binding.root)
}