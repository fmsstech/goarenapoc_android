package com.fmss.goarena.ui.home

import android.view.View
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.fmss.goarena.R
import com.fmss.goarena.base.BaseBindingFragment
import com.fmss.goarena.databinding.FragmentHomeBinding
import com.fmss.goarena.extensions.setOnLoadMoreListener
import com.fmss.goarena.extensions.toast
import com.fmss.goarena.model.Post
import com.fmss.goarena.model.PostGroup
import com.fmss.goarena.network.ResponseState
import com.fmss.goarena.ui.sharePost.ShareEditPostActivity
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : BaseBindingFragment<FragmentHomeBinding>() {

    val vm: HomeViewModel by viewModels()

    @Inject
    lateinit var mAdapter: PostAdapter


    override fun populateUI(rootView: View) {

        mBinding?.recyclerView?.adapter = mAdapter
        mAdapter.uiCallBack = object : PostAdapter.PostListUICallBack {
            override fun onClickEdit(item: Post) {
                context?.run {
                    startActivity(ShareEditPostActivity.newIntent(this, item))

                }
            }

        }
        mBinding?.swipeRefreshLayout?.setOnRefreshListener {
//load refresh
            vm.currentPage = 0
            vm.getPostList()

        }


        vm.postListResponse.observe(this, Observer {
            mBinding?.swipeRefreshLayout?.isRefreshing = false
            when (it) {
                is ResponseState.Loading -> if (it.loading) {

                } else {
                    mBinding?.shimmer?.visibility = View.GONE

                }
                is ResponseState.Error -> {// showErrorDialog()
                    context?.toast(it.errorMessage)
                }
                is ResponseState.Success -> {
                    val data = it.data
                    mAdapter.submitList(data.groupList)
                }
            }
        })


        val postList = mutableListOf<PostGroup>()
        mBinding?.recyclerView?.setOnLoadMoreListener({
            //vm.loadMore()
            vm.getPostList()

        }, visibleThreshold = 1)



        mAdapter.submitList(postList)

    }

    override fun getContentLayoutResId() = R.layout.fragment_home
}