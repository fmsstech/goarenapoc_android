package com.fmss.goarena.data.mock

import android.content.Context
import com.fmss.goarena.util.FileUtil
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by nikinci on 1/30/21.
 */

@Singleton
class MockHelper @Inject constructor(val fileUtil: FileUtil) {

    companion object {
        const val RESPONSE_JSON_PATH_PREFIX = "response/"
    }


    fun <T> getMockResponse(
        context: Context,
        fileName: String,
        clazz: Class<T>,
        success: (T) -> Unit,
        fail: (String) -> Unit, completed: (() -> Unit)? = null
    ) {

        fileUtil.getAssetJsonAs(context, RESPONSE_JSON_PATH_PREFIX + fileName, clazz)?.let {
            success(it)
        } ?: kotlin.run {
            fail("")
        }

        completed?.let { it() }

    }

    fun <T> getMockResponse(
        context: Context,
        fileName: String,
        clazz: Class<T>
    ): T? {

        fileUtil.getAssetJsonAs(context, "$RESPONSE_JSON_PATH_PREFIX$fileName.json", clazz)?.let {
            return it
        }

        return null

    }


}