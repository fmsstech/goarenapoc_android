package com.fmss.goarena.data.remote

import com.fmss.goarena.data.source.UserDataSource
import com.fmss.goarena.network.GoArenaService
import com.fmss.goarena.network.ResponseState
import com.fmss.goarena.network.request.LoginRequestDto
import com.fmss.goarena.network.response.LoginResponseDto
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * Created by nikinci on 1/30/21.
 */
class UserRemoteDataSource @Inject constructor(val service: GoArenaService) :
    UserDataSource {


    override suspend fun login(requestDto: LoginRequestDto): Flow<ResponseState<LoginResponseDto>> =
        flow {
            emit(ResponseState.Loading(true))
            emit(service.login(requestDto))
            emit(ResponseState.Loading(false))
        }
}