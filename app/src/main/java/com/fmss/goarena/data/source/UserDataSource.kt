package com.fmss.goarena.data.source

import com.fmss.goarena.network.ResponseState
import com.fmss.goarena.network.request.LoginRequestDto
import com.fmss.goarena.network.response.LoginResponseDto
import kotlinx.coroutines.flow.Flow

/**
 * Created by nikinci on 1/30/21.
 */
interface UserDataSource {
    suspend fun login(requestDto: LoginRequestDto): Flow<ResponseState<LoginResponseDto>>
}