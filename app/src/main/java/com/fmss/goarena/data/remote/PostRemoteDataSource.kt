package com.fmss.goarena.data.remote

import com.fmss.goarena.data.source.PostDataSource
import com.fmss.goarena.network.GoArenaService
import com.fmss.goarena.network.ResponseState
import com.fmss.goarena.network.request.DashboardRequestDto
import com.fmss.goarena.network.request.PostListRequestDto
import com.fmss.goarena.network.request.PostRequestDto
import com.fmss.goarena.network.response.PostListResponseDto
import com.fmss.goarena.network.response.PostResponseDto
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * Created by nikinci on 1/30/21.
 */
class PostRemoteDataSource @Inject constructor(val service: GoArenaService) :
    PostDataSource {
    override suspend fun getCmsBulk(requestDto: DashboardRequestDto): Flow<ResponseState<PostListResponseDto>> =
        flow {
            emit(ResponseState.Loading(true))
            emit(service.getCmsBulk(requestDto))
            emit(ResponseState.Loading(false))
        }

    override suspend fun getPostList(requestDto: PostListRequestDto): Flow<ResponseState<PostListResponseDto>> =
        flow {
            emit(ResponseState.Loading(true))
            emit(service.getPostList(requestDto))
            emit(ResponseState.Loading(false))
        }

    override suspend fun sendPost(requestDto: PostRequestDto): Flow<ResponseState<PostResponseDto>> =
        flow {
            emit(ResponseState.Loading(true))
            emit(service.sendPost(requestDto))
            emit(ResponseState.Loading(false))
        }

}