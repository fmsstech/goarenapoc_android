package com.fmss.goarena.data.source

import com.fmss.goarena.network.ResponseState
import com.fmss.goarena.network.request.DashboardRequestDto
import com.fmss.goarena.network.request.PostListRequestDto
import com.fmss.goarena.network.request.PostRequestDto
import com.fmss.goarena.network.response.PostListResponseDto
import com.fmss.goarena.network.response.PostResponseDto
import kotlinx.coroutines.flow.Flow

/**
 * Created by nikinci on 1/30/21.
 */
interface PostDataSource {
    suspend fun getCmsBulk(requestDto: DashboardRequestDto): Flow<ResponseState<PostListResponseDto>>
    suspend fun getPostList(requestDto: PostListRequestDto): Flow<ResponseState<PostListResponseDto>>
    suspend fun sendPost(requestDto: PostRequestDto): Flow<ResponseState<PostResponseDto>>
}