package com.fmss.goarena.data.mock

import android.content.Context
import com.fmss.goarena.data.source.PostDataSource
import com.fmss.goarena.network.ResponseState
import com.fmss.goarena.network.request.DashboardRequestDto
import com.fmss.goarena.network.request.PostListRequestDto
import com.fmss.goarena.network.request.PostRequestDto
import com.fmss.goarena.network.response.PostListResponseDto
import com.fmss.goarena.network.response.PostResponseDto
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * Created by nikinci on 1/30/21.
 */
class PostMockDataSource @Inject constructor(
    @ApplicationContext val context: Context, private val mockHelper: MockHelper
) :
    PostDataSource {

    override suspend fun getCmsBulk(requestDto: DashboardRequestDto): Flow<ResponseState<PostListResponseDto>> {

        return flow {
            emit(ResponseState.Loading(true))
            delay(250)
            mockHelper.getMockResponse(
                context,
                "cmsBulk",
                PostListResponseDto::class.java
            )?.let {
                emit(ResponseState.Success(it))
            }
            emit(ResponseState.Loading(false))
        }
    }

    override suspend fun getPostList(requestDto: PostListRequestDto): Flow<ResponseState<PostListResponseDto>> {
        return flow {
            emit(ResponseState.Loading(true))
            delay(250)
            mockHelper.getMockResponse(
                context,
                "postList",
                PostListResponseDto::class.java
            )?.let {
                emit(ResponseState.Success(it))
            }
            emit(ResponseState.Loading(false))
        }
    }

    override suspend fun sendPost(requestDto: PostRequestDto): Flow<ResponseState<PostResponseDto>> {
        return flow {
            emit(ResponseState.Loading(true))
            delay(250)
            mockHelper.getMockResponse(
                context,
                "sendPost",
                PostResponseDto::class.java
            )?.let {
                emit(ResponseState.Success(it))
            }
            emit(ResponseState.Loading(false))
        }
    }
}