package com.fmss.goarena.data.repository

import com.fmss.goarena.data.source.PostDataSource
import com.fmss.goarena.network.ResponseState
import com.fmss.goarena.network.request.DashboardRequestDto
import com.fmss.goarena.network.request.PostListRequestDto
import com.fmss.goarena.network.request.PostRequestDto
import com.fmss.goarena.network.response.PostListResponseDto
import com.fmss.goarena.network.response.PostResponseDto
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * Created by nikinci on 1/30/21.
 */
class PostRepository @Inject constructor(private val dataSource: PostDataSource) : PostDataSource {

    override suspend fun getCmsBulk(requestDto: DashboardRequestDto) =
        dataSource.getCmsBulk(requestDto)

    override suspend fun getPostList(requestDto: PostListRequestDto): Flow<ResponseState<PostListResponseDto>> =
        dataSource.getPostList(requestDto)

    override suspend fun sendPost(requestDto: PostRequestDto): Flow<ResponseState<PostResponseDto>> =
        dataSource.sendPost(requestDto)

}