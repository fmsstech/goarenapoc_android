package com.fmss.goarena.data.repository

import com.fmss.goarena.data.source.UserDataSource
import com.fmss.goarena.network.ResponseState
import com.fmss.goarena.network.request.LoginRequestDto
import com.fmss.goarena.network.response.LoginResponseDto
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * Created by nikinci on 1/30/21.
 */
class UserRepository @Inject constructor(private val dataSource: UserDataSource) : UserDataSource {

    override suspend fun login(requestDto: LoginRequestDto): Flow<ResponseState<LoginResponseDto>> {
        return dataSource.login(requestDto)
    }
}