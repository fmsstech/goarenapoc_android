package com.fmss.goarena.data.mock

import android.content.Context
import com.fmss.goarena.data.source.UserDataSource
import com.fmss.goarena.network.ResponseState
import com.fmss.goarena.network.request.LoginRequestDto
import com.fmss.goarena.network.response.LoginResponseDto
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * Created by nikinci on 1/30/21.
 */
class UserMockDataSource @Inject constructor(
    @ApplicationContext val context: Context, private val mockHelper: MockHelper
) :
    UserDataSource {

    override suspend fun login(requestDto: LoginRequestDto): Flow<ResponseState<LoginResponseDto>> {
        return flow {
            emit(ResponseState.Loading(true))
            delay(250)
            mockHelper.getMockResponse(
                context,
                "login",
                LoginResponseDto::class.java
            )?.let {
                emit(ResponseState.Success(it))
            }
            emit(ResponseState.Loading(false))
        }
    }
}