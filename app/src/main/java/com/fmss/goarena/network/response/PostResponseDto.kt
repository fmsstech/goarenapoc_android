package com.fmss.goarena.network.response

class PostResponseDto(
    var id: Long? = null,
    var title: String? = null,
    var description: String? = null,
    var imageStr: String? = null,
    var imageUrl: String? = null
) : BaseResponseDto()