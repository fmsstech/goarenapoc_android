package com.fmss.goarena.network.response

/**
 * Created by nikinci on 1/30/21.
 */

class RestResponse<T> {
    var status: ResponseStatus? = null
    var data: T? = null

    constructor  () {

    }

    private constructor(builder: RestResponse.Builder<T>) {
        status = builder.status
        data = builder.content
    }


    class Builder<T> {
        var status: ResponseStatus? = null
        var content: T? = null
        fun status(status: ResponseStatus?): RestResponse.Builder<T> {
            this.status = status
            return this
        }

        fun content(content: T): RestResponse.Builder<T> {
            this.content = content
            return this
        }

        fun build(): RestResponse<T> {
            return RestResponse(this)
        }
    }
}
