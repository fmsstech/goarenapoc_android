package com.fmss.goarena.network

import com.fmss.goarena.network.response.RestResponse
import okhttp3.Request
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type
import java.util.concurrent.Executor


class RestResponseCall<T : Any>(
    private val delegate: Call<RestResponse<T>>,
    private val successType: Type,
    private val callbackExecutor: Executor?
) : Call<ResponseState<T>> {
    override fun enqueue(callback: Callback<ResponseState<T>>) {
        return delegate.enqueue(object : Callback<RestResponse<T>> {
            override fun onResponse(
                call: Call<RestResponse<T>>,
                response: Response<RestResponse<T>>
            ) {
                if (response.isSuccessful) {
                    val restResponse = response.body()
                    if (restResponse == null) {
                        val errorText = "response body null"
                        callback.onResponse(
                            this@RestResponseCall,
                            Response.success(ResponseState.Error(errorText))
                        )
                        return
                    }

                    val responseStatus = restResponse.status
                    when (responseStatus?.code) {
                        NetworkConstants.RESPONSE_CODE_SUCCESS -> {
                            callback.onResponse(
                                this@RestResponseCall,
                                Response.success(ResponseState.Success(restResponse.data as T))
                            )
                        }
                        else -> {
                            val errorText = responseStatus?.message ?: "error"
                            callback.onResponse(
                                this@RestResponseCall,
                                Response.success(ResponseState.Error(errorText))
                            )
                        }
                    }
                } else {
                    callback.onResponse(
                        this@RestResponseCall,
                        Response.success(ResponseState.Error("http status error"))
                    )
                }
            }

            override fun onFailure(call: Call<RestResponse<T>>, t: Throwable) {

                if (!isCanceled) {
                    val errorText = "network error"

                    callback.onResponse(
                        this@RestResponseCall,
                        Response.success(ResponseState.Error(errorText))
                    )
                }
            }

        })
    }

    override fun isExecuted() = delegate.isExecuted

    override fun clone() = RestResponseCall(delegate.clone(), successType, callbackExecutor)

    override fun isCanceled() = delegate.isCanceled

    override fun cancel() = delegate.cancel()

    override fun execute(): Response<ResponseState<T>> {
        throw UnsupportedOperationException("NetworkResponseCall doesn't support execute")
    }

    override fun request(): Request = delegate.request()

}