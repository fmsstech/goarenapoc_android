package com.fmss.goarena.network.request

class PostRequestDto(
    var id: Long? = null,
    var title: String,
    var description: String,
    var imageStr: String? = null
) : BaseRequestDto()