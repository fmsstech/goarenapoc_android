package com.fmss.goarena.network

import com.fmss.goarena.network.response.BaseResponseDto
import com.fmss.goarena.network.response.RestResponse
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import java.util.concurrent.Executor

/**
 * Created by nikinci on 1/30/21.
 */

class RestResponseCallAdapter<T : Any>(
    private val responseType: Type,
    private val callbackExecutor: Executor?
) :
    CallAdapter<RestResponse<T>, Call<ResponseState<T>>> {

    override fun responseType(): Type = responseType

    override fun adapt(call: Call<RestResponse<T>>): Call<ResponseState<T>> =
        RestResponseCall(call, responseType, callbackExecutor)
}

class RestResponseCallAdapterFactory private constructor() : CallAdapter.Factory() {
    override fun get(
        returnType: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {
        // suspend functions wrap the response type in `Call`
        if (Call::class.java != getRawType(returnType)) {
            return null
        }

        // check first that the return type is `ParameterizedType`
        check(returnType is ParameterizedType) {
            "return type must be parameterized as Call<Foo> or Call<out Foo>"
        }

        // get the response type inside the `Call` type
        val responseStateType = getParameterUpperBound(0, returnType)
        // if the response type is not ApiResponse then we can't handle this type, so we return null
        if (getRawType(responseStateType) != ResponseState::class.java) {
            return null
        }

        // the response type is ApiResponse and should be parameterized
        check(responseStateType is ParameterizedType) { "Response must be parameterized as ResponseState<Foo> or ResponseState<out Foo>" }

        // get the response type inside the `ResponseState` type
        val responseType = getParameterUpperBound(0, responseStateType)
        // if the response type is not ApiResponse then we can't handle this type, so we return null
        if (getRawType(responseType).superclass != BaseResponseDto::class.java) {
            return null
        }

//        val successBodyType = getParameterUpperBound(0, responseType)

        val restResponseType = object : ParameterizedType {
            override fun getActualTypeArguments(): Array<Type> = arrayOf(
                responseType
            )

            override fun getRawType(): Type {
                return RestResponse::class.java
            }

            override fun getOwnerType(): Type? {
                return null
            }
        }

        return RestResponseCallAdapter<RestResponse<Any>>(
            restResponseType,
            retrofit.callbackExecutor()
        )
    }

    companion object {
        @JvmStatic
        fun create() = RestResponseCallAdapterFactory()
    }
}
