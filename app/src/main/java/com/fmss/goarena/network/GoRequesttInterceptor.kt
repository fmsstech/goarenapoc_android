package com.fmss.goarena.network

import com.fmss.goarena.core.UserManager
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by nikinci on 2/1/21.
 */
class GoRequesttInterceptor(val userManager: UserManager) : Interceptor {

    companion object {
        const val AUTH_FORMAT = "Bearer %s"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        userManager.authToken.value?.let {

            val originalRequest = chain.request()
            val requestWithAuth = originalRequest.newBuilder()
                .header("Authorization", AUTH_FORMAT.format(it))
                .build()
            return chain.proceed(requestWithAuth)
        }
        return chain.proceed(chain.request())
    }

}