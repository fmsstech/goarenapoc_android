package com.fmss.goarena.network.request

class PostListRequestDto(
    var page: Int
) : BaseRequestDto()