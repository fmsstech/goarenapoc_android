package com.fmss.goarena.network.response

class LoginResponseDto(
    var token: String? = null,
    var username: String? = null,
    var id: Long? = null
) : BaseResponseDto()

