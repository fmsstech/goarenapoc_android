package com.fmss.goarena.network

import com.fmss.goarena.network.request.DashboardRequestDto
import com.fmss.goarena.network.request.LoginRequestDto
import com.fmss.goarena.network.request.PostListRequestDto
import com.fmss.goarena.network.request.PostRequestDto
import com.fmss.goarena.network.response.LoginResponseDto
import com.fmss.goarena.network.response.PostListResponseDto
import com.fmss.goarena.network.response.PostResponseDto
import retrofit2.http.Body
import retrofit2.http.POST

interface GoArenaService {

    @POST("/turkcellim_ios/guest/cmsBulk.json")
    suspend fun getCmsBulk(@Body requestDto: DashboardRequestDto): ResponseState<PostListResponseDto>

    @POST("/user-service/login")
    suspend fun login(@Body requestDto: LoginRequestDto): ResponseState<LoginResponseDto>


    @POST("/post-service/post/find-all-confirmed")
    suspend fun getPostList(@Body requestDto: PostListRequestDto): ResponseState<PostListResponseDto>

    @POST("/post-service/post")
    suspend fun sendPost(@Body requestDto: PostRequestDto): ResponseState<PostResponseDto>
}