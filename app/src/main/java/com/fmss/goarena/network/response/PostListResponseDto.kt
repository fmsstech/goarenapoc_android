package com.fmss.goarena.network.response

import com.fmss.goarena.model.PostGroup

class PostListResponseDto(
    var groupList: MutableList<PostGroup>? = null
) : BaseResponseDto()