package com.fmss.goarena.network.request

data class LoginRequestDto(var username: String, var password: String)