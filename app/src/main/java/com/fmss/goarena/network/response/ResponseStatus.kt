package com.fmss.goarena.network.response

import com.fmss.goarena.network.BaseDto

/**
 * Created by nikinci on 1/30/21.
 */
class ResponseStatus : BaseDto {
    var message: String? = null
    var code: String? = null

    private var logId: String? = null

    constructor() {}
    constructor(code: String?, message: String?) {
        this.code = code
        this.message = message
    }

    constructor(code: String?, message: String?, logId: String?) {
        this.code = code
        this.message = message
        this.logId = logId
    }


    fun getLogId(): String? {
        return logId
    }

    fun setLogId(logId: String?) {
        this.logId = logId
    }

    override fun toString(): String {
        val builder = StringBuilder()
        builder.append("Status [message=")
        builder.append(message)
        builder.append(", code=")
        builder.append(code)
        builder.append("]")
        return builder.toString()
    }

    companion object {
        private const val serialVersionUID = 1L
    }
}
