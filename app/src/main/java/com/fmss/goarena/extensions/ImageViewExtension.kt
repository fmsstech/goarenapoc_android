package com.fmss.goarena.extensions

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.drawable.Drawable
import android.util.Base64
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.load
import coil.transform.CircleCropTransformation

/**
 * Created by nikinci on 2/1/21.
 */

@BindingAdapter(
    value = ["app:imageUrl", "app:placeholder", "app:error", "app:circle", "app:crossfade"],
    requireAll = false
)
fun ImageView.bindimageUrl(
    imageUrl: String?,
    placeholder: Drawable? = null,
    error: Drawable? = null,
    circle: Boolean = false,
    crossfade: Boolean = true
) {
    imageUrl?.let {

        try {

            load(it) {
                crossfade(crossfade)
                error?.run {
                    error((this))
                }
                placeholder?.run {
                    placeholder(this)
                }
                if (circle) {
                    transformations(CircleCropTransformation())
                }
                bitmapConfig(Bitmap.Config.ARGB_8888)
            }
        } catch (e: Exception) {
        }


    }
}


@BindingAdapter(
    value = ["app:imageStr"]
)
fun ImageView.bindImageStr(
    imageStr: String?
) {
    imageStr?.let {

        try {

            val imageBytes = Base64.decode(it, Base64.DEFAULT)
            val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
            setImageBitmap(decodedImage)

        } catch (e: Exception) {
            e.message
        }


    }
}
