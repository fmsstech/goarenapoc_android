package com.fmss.goarena.extensions

import android.net.Uri
import android.os.Build
import androidx.core.net.toFile
import java.util.*

/**
 * Created by nikinci on 2/1/21.
 */
fun Uri.toBase64(): String {
    val bytes = toFile().readBytes()
    val base64 = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        Base64.getEncoder().encodeToString(bytes)
    } else {
        android.util.Base64.encodeToString(bytes, android.util.Base64.DEFAULT)
    }


    return base64
}