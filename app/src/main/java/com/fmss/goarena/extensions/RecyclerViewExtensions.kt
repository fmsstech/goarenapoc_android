package com.fmss.goarena.extensions

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fmss.goarena.R
import com.fmss.goarena.util.RecyclerDividerItemDecoration
import com.fmss.goarena.util.RecyclerMarginItemDecoration

/**
 * Created by nikinci on 2/1/21.
 */
object RecyclerViewExtensions {
    enum class RecylerViewDecorationType {
        MARGIN, DIVIDER;

        companion object {
            fun getFromString(text: String): RecylerViewDecorationType =
                if (text.equals(DIVIDER.name, true)) {
                    MARGIN
                } else {
                    DIVIDER
                }
        }
    }

    enum class RecyclerViewLinearLayManagerType {
        VERTICAL, HORIZONTAL;
    }
}


@BindingAdapter(
    value = ["itemDecorator"],
    requireAll = true
)
fun RecyclerView.setItemDecorator(type: RecyclerViewExtensions.RecylerViewDecorationType) {
    when (type) {
        RecyclerViewExtensions.RecylerViewDecorationType.DIVIDER -> {
            var orientation = RecyclerDividerItemDecoration.VERTICAL
            layoutManager?.let {
                if (it.canScrollHorizontally()) {
                    orientation = RecyclerDividerItemDecoration.HORIZONTAL
                }
            }
            addItemDecoration(
                RecyclerDividerItemDecoration(context, orientation)
            )
        }
        RecyclerViewExtensions.RecylerViewDecorationType.MARGIN -> {
            var orientation = RecyclerDividerItemDecoration.VERTICAL
            layoutManager?.let {
                if (it.canScrollHorizontally()) {
                    orientation = RecyclerDividerItemDecoration.HORIZONTAL
                }
            }
            addItemDecoration(
                RecyclerMarginItemDecoration(
                    resources.getDimension(R.dimen.app_padding_small).toInt(), orientation
                )
            )
        }
    }
}


fun RecyclerView.setOnLoadMoreListener(
    loadMore: () -> Unit,
    visibleThreshold: Int = 5
) {
    addOnScrollListener(object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)

            if (layoutManager == null) {
                throw IllegalStateException("layoutManager is null")
            }

            if (layoutManager is LinearLayoutManager) {
                val totalItemCount = layoutManager!!.itemCount
                val lastVisibleItemPosition =
                    (layoutManager as LinearLayoutManager).findLastVisibleItemPosition()
                if (totalItemCount <= (lastVisibleItemPosition + visibleThreshold)) {
                    loadMore()
                }
            } else {
                throw IllegalStateException("layoutManager not a  LinearLayoutManager")
            }
        }
    })
}