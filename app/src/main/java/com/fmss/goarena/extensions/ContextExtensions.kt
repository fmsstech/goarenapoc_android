package com.fmss.goarena.extensions

import android.app.Dialog
import android.content.Context
import android.widget.Toast
import com.fmss.goarena.ui.loading.GoArenaLoadingDialog

/**
 * Created by nikinci on 2/1/21.
 */
fun Context.showLoadingDialog(): Dialog {
    return GoArenaLoadingDialog(this).apply {
        show()
    }
}

fun Context.toast(message: String?, duration: Int = Toast.LENGTH_SHORT) {
    message?.let {
        Toast.makeText(this, it, duration).show()
    }
}