package com.fmss.goarena.util

import android.content.Context
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by nikinci on 1/30/21.
 */

@Singleton
class FileUtil @Inject constructor(val objectMapper: ObjectMapper) {
    fun <T> getAssetJsonAs(
        context: Context,
        fileName: String,
        clazz: Class<T>
    ): T? {
        getAssetJsonAsString(context, fileName)?.let {
            return try {
                objectMapper.readValue(it, clazz)
            } catch (e: Exception) {
                null
            }
        }

        return null
    }


    fun getAssetJsonAsString(context: Context, filename: String): String? {
        val json: String
        try {
            var readFileName = filename
            if (!readFileName.endsWith(".json")) {
                readFileName = "$readFileName.json"
            }
            val inputStream = context.assets.open(readFileName)
            val size = inputStream.available()
            val buffer = ByteArray(size)
            inputStream.use { it.read(buffer) }
            json = String(buffer)
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return json
    }
}
