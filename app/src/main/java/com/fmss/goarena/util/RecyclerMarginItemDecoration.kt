package com.fmss.goarena.util

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * Created by nikinci on 2/1/21.
 */
class RecyclerMarginItemDecoration(
    private val spaceHeight: Int,
    private val mOrientation: Int = RecyclerDividerItemDecoration.VERTICAL
) : RecyclerView.ItemDecoration() {


    override fun getItemOffsets(
        outRect: Rect, view: View,
        parent: RecyclerView, state: RecyclerView.State
    ) {
        with(outRect) {
            when (mOrientation) {
                RecyclerDividerItemDecoration.VERTICAL -> {
                    if (parent.getChildAdapterPosition(view) == 0) {
                        top = spaceHeight
                    }
                    bottom = spaceHeight
                }
                RecyclerDividerItemDecoration.HORIZONTAL -> {
                    if (parent.getChildAdapterPosition(view) > 0) {
                        left = spaceHeight
                    }
                    right = spaceHeight
                }
            }


        }
    }
}