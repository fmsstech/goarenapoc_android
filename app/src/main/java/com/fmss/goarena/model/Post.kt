package com.fmss.goarena.model

import java.io.Serializable

/**
 * Created by nikinci on 2/1/21.
 */
class Post(
    var id: Long? = null,
    var title: String? = null,
    var description: String? = null,
    var imageStr: String? = null,
    var imageUrl: String? = null
) : Serializable