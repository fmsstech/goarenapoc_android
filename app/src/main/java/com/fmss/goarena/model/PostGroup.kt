package com.fmss.goarena.model

import java.io.Serializable

/**
 * Created by nikinci on 2/1/21.
 */
class PostGroup(var post: Post? = null, var user: User? = null) : Serializable