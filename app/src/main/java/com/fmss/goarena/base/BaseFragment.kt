package com.fmss.goarena.base

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by nikinci on 2/1/21.
 */
abstract class BaseBindingFragment<T : ViewDataBinding> : BaseFragment() {
    /**
     *
     * Binding object related to this fragment instance.
     */
    protected var mBinding: T? = null
    protected var mDialog: Dialog? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(
            inflater, getContentLayoutResId(), container, false
        )
        mBinding?.lifecycleOwner = viewLifecycleOwner
        return mBinding?.root
    }
}

@AndroidEntryPoint
abstract class BaseFragment : Fragment() {
    protected abstract fun populateUI(rootView: View)

    protected open fun getScreenName(): String? {
        return null
    }


    open fun getFragmentTag(): String {
        return javaClass.simpleName
    }

    @LayoutRes
    protected abstract fun getContentLayoutResId(): Int


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(getContentLayoutResId(), container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populateUI(view)
    }


    protected fun showLoading() {
        (activity as BaseActivity).showLoading()
    }

    protected fun hideLoading() {
        (activity as BaseActivity).hideLoading()
    }


}
