package com.fmss.goarena.base

import androidx.lifecycle.ViewModel

/**
 * Created by nikinci on 1/30/21.
 */
abstract class BaseViewModel : ViewModel()