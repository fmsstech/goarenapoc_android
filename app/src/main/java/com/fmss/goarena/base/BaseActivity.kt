package com.fmss.goarena.base

import android.app.Dialog
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.fmss.goarena.extensions.showLoadingDialog

/**
 * Created by nikinci on 1/30/21.
 */
abstract class BaseBindingActivity<T : ViewDataBinding>() :
    BaseActivity() {

    protected var mBinding: T? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, getContentViewLayoutResId())
        mBinding?.lifecycleOwner = this
        populateUI(savedInstanceState)
    }
}


abstract class BaseActivity() : AppCompatActivity() {
    protected abstract fun populateUI(savedInstanceState: Bundle?)

    @LayoutRes
    protected abstract fun getContentViewLayoutResId(): Int
    protected var mDialog: Dialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (this !is BaseBindingActivity<*>) {
            setContentView(getContentViewLayoutResId())
            populateUI(savedInstanceState)
        }
    }

    fun showLoading() {
        hideLoading()
        mDialog = showLoadingDialog()
    }

    fun hideLoading() {
        mDialog?.dismiss()
    }

}
